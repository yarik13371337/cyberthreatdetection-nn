import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np


class CyberThreatDetectionModel(nn.Module):
    def __init__(self, input_dim):
        super(CyberThreatDetectionModel, self).__init__()
        self.fc1 = nn.Linear(input_dim, 64)
        self.fc2 = nn.Linear(64, 32)
        self.fc3 = nn.Linear(32, 16)
        self.fc4 = nn.Linear(16, 1)
        self.dropout = nn.Dropout(0.5)
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        x = self.relu(self.fc1(x))
        x = self.dropout(x)
        x = self.relu(self.fc2(x))
        x = self.dropout(x)
        x = self.relu(self.fc3(x))
        x = self.fc4(x)
        x = self.sigmoid(x)
        return x


data = pd.read_csv("cyber_threat_data.csv")
X = data.drop("label", axis=1)
y = data["label"]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)
X_train, X_test = torch.tensor(X_train, dtype=torch.float32), torch.tensor(X_test, dtype=torch.float32)
y_train, y_test = torch.tensor(y_train.values, dtype=torch.float32).view(-1, 1), torch.tensor(
    y_test.values, dtype=torch.float32
).view(-1, 1)

input_dim = X_train.shape[1]
model = CyberThreatDetectionModel(input_dim)
criterion = nn.BCELoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)
num_epochs = 50
batch_size = 32

for epoch in range(num_epochs):
    model.train()
    permutation = torch.randperm(X_train.size()[0])
    for i in range(0, X_train.size()[0], batch_size):
        indices = permutation[i : i + batch_size]
        batch_x, batch_y = X_train[indices], y_train[indices]
        optimizer.zero_grad()
        outputs = model(batch_x)
        loss = criterion(outputs, batch_y)
        loss.backward()
        optimizer.step()

model.eval()
with torch.no_grad():
    outputs = model(X_test)
    predicted = (outputs > 0.5).float()
    accuracy = (predicted == y_test).float().mean()
    print(f"Test Accuracy: {accuracy * 100:.2f}%")

torch.save(model.state_dict(), "cyber_threat_detection_model.pth")

model = CyberThreatDetectionModel(input_dim)
model.load_state_dict(torch.load("cyber_threat_detection_model.pth"))
model.eval()

new_data = scaler.transform(np.array([[0.5, 0.2, 0.1, 0.3]]))
new_data = torch.tensor(new_data, dtype=torch.float32)
with torch.no_grad():
    prediction = model(new_data)
    print(f"Prediction: {prediction[0].item()}")
